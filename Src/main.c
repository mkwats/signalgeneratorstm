/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32l4xx_hal.h"
#include "adc.h"
#include "dac.h"
#include "dma.h"
#include "i2c.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* USER CODE BEGIN Includes */
#include "u8g2.h"

/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
static DAC_ChannelConfTypeDef sConfig;

const uint16_t DAC_SIGNAL_Sinus[]= {
	2047, 2447, 2831, 3185, 3498, 3750, 3939, 4056,
	4095, 4056, 3939, 3750, 3495, 3185, 2831, 2447,
	2047, 1647, 1263, 909, 599, 344, 155, 38,
	0, 38, 155, 344, 599, 909, 1263, 1647
};

const uint16_t DAC_SIGNAL_Triangle[] = {
	0, 256, 512, 768, 1024, 1279, 1535, 1791,
	2047, 2303, 2559, 2815, 3071, 3326, 3582, 3838,
	4095, 3838, 3582, 3326, 3071, 2815, 2559, 2303,
	2047, 1791, 1535, 1279, 1024, 768, 512, 256
};

const uint16_t DAC_SIGNAL_Sawtooth[] = {
	0, 132, 264, 396, 528, 660, 792, 924,
	1057, 1189, 1321, 1453, 1585, 1717, 1849, 1981,
	2113, 2245, 2377, 2509, 2641, 2773, 2905, 3037,
	3170, 3302, 3434, 3566, 3698, 3830, 3962, 4095
};

const uint16_t DAC_SIGNAL_Square[] = {
	0, 3000 //4095
};

uint16_t adcData[1];


//u8g2 stuff
u8g2_t u8g2_iic;


#define UNUSEDVAR __attribute__ ((unused))

typedef enum {
  HI_WORLD,
  LYDIA_PIC,
  STOCK_U8G2,
  TIMECODE,
  LINES,
  LAST_IMG
} IMGS;

void byte_at_string(uint8_t *str, int i)
{

  str[2] = '0' + i%10;
  str[1] = '0' + (i/10)%10;
  str[0] = '0' + (i/100)%10;

}
void btoa(uint8_t i, uint8_t *str)
{
  str[2] = '0' + i%10;
  str[1] = '0' + (i/10)%10;
  str[0] = '0' + (i/100)%10;
}

void itoa(uint32_t i, uint8_t *str)
{
  str[4] = '0' + i%10;
  str[3] = '0' + (i/10)%10;
  str[2] = '0' + (i/100)%10;
  str[1] = '0' + (i/1000)%10;
  str[0] = '0' + (i/10000)%10;
}

void hex_at_string(uint8_t *str, int i)
{
  uint8_t lower = i&0xF;
  uint8_t upper = (i&0xF0) >> 4;

  if (lower < 10) {
    str[1] = '0' + lower;
  } else {
    str[1] = 'A' + lower - 10;
  }
  if (upper < 10) {
    str[0] = '0' + upper;
  } else {
    str[0] = 'A' + upper - 10;
  }
}


int my_I2C_GET_FLAG(I2C_HandleTypeDef* __HANDLE__,
                    uint32_t __FLAG__)
{
	HAL_Delay(100);
  // #define I2C_FLAG_BUSY                   ((uint32_t)0x0010 0002)
  // #define I2C_FLAG_MASK                   ((uint32_t)0x0000 FFFF)
	/*
  if (((uint8_t)((__FLAG__) >> 16)) == 0x01) {
    return (__HANDLE__->Instance->SR1 & __FLAG__ & I2C_FLAG_MASK) == (__FLAG__ & I2C_FLAG_MASK);
  } else {
    return (__HANDLE__->Instance->SR2 & __FLAG__ & I2C_FLAG_MASK) == (__FLAG__ & I2C_FLAG_MASK);
  }
  */
	return 1;
}

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_NVIC_Init(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
static void DAC_Ch1_SineConfig(void);
static void DAC_Ch1_SquareWithNoise(void);
static void DAC_Ch1_TriangleConfig(void);

//pwm signal stuff
float iDUTY = 0	;
int iSetFreq = 0;

uint32_t amountsent =0;
uint8_t u8x8_byte_my_hw_i2c(
  U8X8_UNUSED u8x8_t *u8x8,
  U8X8_UNUSED uint8_t msg,
  U8X8_UNUSED uint8_t arg_int,
  U8X8_UNUSED void *arg_ptr);

uint8_t u8x8_gpio_and_delay_mine(
u8x8_t *u8x8,
uint8_t msg,
uint8_t arg_int,
void *arg_ptr);

uint8_t USER_UpdatePWM(void);
void Refresh_OLED(u8g2_t *u8g2, IMGS img, char* data);
uint8_t USER_SetFrequency(void);

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

int main(void)
{

  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_DAC1_Init();
  MX_TIM4_Init();
  MX_TIM6_Init();
  MX_ADC1_Init();
  MX_TIM8_Init();
  MX_USART2_UART_Init();
  MX_TIM1_Init();
  MX_I2C1_Init();

  /* Initialize interrupts */
  MX_NVIC_Init();

  /* USER CODE BEGIN 2 */
//SSD


  u8g2_Setup_ssd1306_i2c_128x64_noname_f(&u8g2_iic,
                                         U8G2_R0,
                                         u8x8_byte_my_hw_i2c,
                                         u8x8_gpio_and_delay_mine);
  u8g2_InitDisplay(&u8g2_iic); // send init sequence to the display, display is in sleep mode after this,
  u8g2_SetPowerSave(&u8g2_iic, 0); // wake up display

  Refresh_OLED(&u8g2_iic, HI_WORLD, NULL);



/*signal generation*/
  HAL_TIM_Base_Start(&htim6);
  DAC_Ch1_SineConfig();

  HAL_DAC_DeInit(&hdac1);
  DAC_Ch1_SquareWithNoise();

  HAL_DAC_DeInit(&hdac1);
  DAC_Ch1_TriangleConfig();


  //PWM signal
  HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_1);
  iDUTY = 50; //%

  //ADC
  HAL_TIM_Base_Start(&htim8);
  HAL_TIM_OC_Start(&htim8, TIM_CHANNEL_1);
  HAL_ADC_Start_DMA(&hadc1, (uint32_t*)&adcData, 1);

  __IO int timerCounter = 0;
  HAL_TIM_Encoder_Start(&htim1, TIM_CHANNEL_ALL);

  //Encoder value
  timerCounter =__HAL_TIM_GET_COUNTER(&htim1)>>2;
  __HAL_TIM_SET_AUTORELOAD(&htim1, 400);

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */
	  if (USER_UpdatePWM() == 1){

		  enum {BufSize=9};
		  char buf[BufSize];
		  snprintf (buf, BufSize, "%d",  iSetFreq );
		  Refresh_OLED(&u8g2_iic, TIMECODE, buf);

	  }

  }
  /* USER CODE END 3 */

}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 1;
  RCC_OscInitStruct.PLL.PLLN = 10;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV7;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_3) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART2|RCC_PERIPHCLK_I2C1
                              |RCC_PERIPHCLK_ADC;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_PCLK1;
  PeriphClkInit.AdcClockSelection = RCC_ADCCLKSOURCE_PLLSAI1;
  PeriphClkInit.PLLSAI1.PLLSAI1Source = RCC_PLLSOURCE_HSI;
  PeriphClkInit.PLLSAI1.PLLSAI1M = 1;
  PeriphClkInit.PLLSAI1.PLLSAI1N = 8;
  PeriphClkInit.PLLSAI1.PLLSAI1P = RCC_PLLP_DIV7;
  PeriphClkInit.PLLSAI1.PLLSAI1Q = RCC_PLLQ_DIV2;
  PeriphClkInit.PLLSAI1.PLLSAI1R = RCC_PLLR_DIV2;
  PeriphClkInit.PLLSAI1.PLLSAI1ClockOut = RCC_PLLSAI1_ADC1CLK;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the main internal regulator output voltage 
    */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/** NVIC Configuration
*/
static void MX_NVIC_Init(void)
{
  /* EXTI15_10_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(EXTI15_10_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);
  /* TIM4_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(TIM4_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(TIM4_IRQn);
}

/* USER CODE BEGIN 4 */
static void DAC_Ch1_SineConfig(void)
{
  /*##-1- Initialize the DAC peripheral ######################################*/
  if (HAL_DAC_Init(&hdac1) != HAL_OK)
  {
    /* Initialization Error */
    Error_Handler();
  }

  /*##-1- DAC channel1 Configuration #########################################*/
  sConfig.DAC_Trigger = DAC_TRIGGER_T6_TRGO;
  sConfig.DAC_OutputBuffer = DAC_OUTPUTBUFFER_ENABLE;

  if (HAL_DAC_ConfigChannel(&hdac1, &sConfig,  DAC_CHANNEL_1) != HAL_OK)
  {
    /* Channel configuration Error */
    Error_Handler();
  }



  if (HAL_DAC_Start_DMA(&hdac1,  DAC_CHANNEL_1, (uint32_t *)DAC_SIGNAL_Sinus, 32, DAC_ALIGN_12B_R) != HAL_OK)
  {

    Error_Handler();
  }

}

static void DAC_Ch1_SquareWithNoise(void){

	  /*##-1- Initialize the DAC peripheral ######################################*/
	  if (HAL_DAC_Init(&hdac1) != HAL_OK)
	  {
	    /* Initialization Error */
	    Error_Handler();
	  }

	  /*##-1- DAC channel1 Configuration #########################################*/
	  sConfig.DAC_Trigger = DAC_TRIGGER_T6_TRGO;
	  sConfig.DAC_OutputBuffer = DAC_OUTPUTBUFFER_ENABLE;


	  /*##-3- DAC channel2 Triangle Wave generation configuration ################*/
	  if (HAL_DACEx_NoiseWaveGenerate(&hdac1, DAC_CHANNEL_1, DAC_LFSRUNMASK_BITS9_0) != HAL_OK)
	  {
	    Error_Handler();
	  }

	  /*start DAD*/
	  if (HAL_DAC_Start_DMA(&hdac1,  DAC_CHANNEL_1, (uint32_t *)DAC_SIGNAL_Square, 2, DAC_ALIGN_12B_R) != HAL_OK)
	  {
	    Error_Handler();
	  }

}

static void DAC_Ch1_TriangleConfig(void)
{
  /*##-1- Initialize the DAC peripheral ######################################*/
  if (HAL_DAC_Init(&hdac1) != HAL_OK)
  {
    /* DAC initialization Error */
    Error_Handler();
  }

  /*##-2- DAC channel2 Configuration #########################################*/
  sConfig.DAC_Trigger = DAC_TRIGGER_T6_TRGO;
  sConfig.DAC_OutputBuffer = DAC_OUTPUTBUFFER_ENABLE;

  if (HAL_DAC_ConfigChannel(&hdac1, &sConfig, DAC_CHANNEL_1) != HAL_OK)
  {
    /* Channel configuration Error */
    Error_Handler();
  }

  /*##-3- DAC channel2 Triangle Wave generation configuration ################*/
  if (HAL_DACEx_TriangleWaveGenerate(&hdac1, DAC_CHANNEL_1, DAC_TRIANGLEAMPLITUDE_1023) != HAL_OK)
  {
    /* Triangle wave generation Error */
    Error_Handler();
  }

  /*##-4- Enable DAC Channel1 ################################################*/
  if (HAL_DAC_Start(&hdac1, DAC_CHANNEL_1) != HAL_OK)
  {
    /* Start Error */
    Error_Handler();
  }

  /*##-5- Set DAC channel1 DHR12RD register ################################################*/
  if (HAL_DAC_SetValue(&hdac1, DAC_CHANNEL_1, DAC_ALIGN_12B_R, 0x100) != HAL_OK)
  {
    /* Setting value Error */
    Error_Handler();
  }
}


uint8_t
u8x8_byte_my_hw_i2c(
  U8X8_UNUSED u8x8_t *u8x8,
  U8X8_UNUSED uint8_t msg,
  U8X8_UNUSED uint8_t arg_int,
  U8X8_UNUSED void *arg_ptr)
{
#define MAX_LEN 32
  static uint8_t vals[MAX_LEN];
  static uint8_t length=0;

  U8X8_UNUSED uint8_t *args = arg_ptr;
  switch(msg)  {
  case U8X8_MSG_BYTE_SEND: {
    if ((arg_int+length) <= MAX_LEN) {
      for(int i=0; i<arg_int; i++) {
        vals[length] = args[i];
        length++;
      }
    } else {
      uint8_t umsg[] = "MSG_BYTE_SEND arg too long xxx\n";
      byte_at_string(umsg+27, arg_int);
      //sendUARTmsgPoll(umsg, sizeof(umsg));
    }
    uint8_t umsg[] = "MSG_BYTE_SEND 0xxx\n";
    // 00 AE = display off
    // 00 D5 = set display clock
    // 00 80    divide ratio
    // 00 A8 = set multiplex ratio
    // 00 3F    64 mux
    // 00 D3 = set display offset
    // 00 00
    // 00 40 - 00 7F set display start line


    hex_at_string(umsg+16, args[0]);
    //sendUARTmsgPoll(umsg, sizeof(umsg));
    break;
  }
  case U8X8_MSG_BYTE_INIT: {
    uint8_t umsg[] = "MSG_BYTE_INIT xxx\n";
    byte_at_string(umsg+14, arg_int);
    //sendUARTmsgPoll(umsg, sizeof(umsg));
    break;
  }
  case U8X8_MSG_BYTE_SET_DC: {
    uint8_t umsg[] = "MSG_BYTE_SET_DC xxx\n";
    byte_at_string(umsg+15, arg_int);
    //sendUARTmsgPoll(umsg, sizeof(umsg));
    break;
  }
  case U8X8_MSG_BYTE_START_TRANSFER: {
    UNUSEDVAR uint8_t umsg[] = "MSG_BYTE_START\n";
    //sendUARTmsgPoll(umsg, sizeof(umsg));
    length = 0;
    break;
  }
  case U8X8_MSG_BYTE_END_TRANSFER: {
    UNUSEDVAR uint8_t umsg[] = "MSG_BYTE_END xxxxx\n";
    itoa(length, umsg+13);
    //sendUARTmsgPoll(umsg, sizeof(umsg));
    while(HAL_I2C_GetState (&hi2c1) != HAL_I2C_STATE_READY) { /* empty */ }
    const uint8_t addr = 0x78;
    HAL_I2C_Master_Transmit(&hi2c1, addr, vals, length, 10);
    amountsent+= length;
    break;
  }
  default: {
    uint8_t umsg[] = "MSG_BYTE_DEFAULT xxx\n";
    byte_at_string(umsg+17, arg_int);
    //sendUARTmsgPoll(umsg, sizeof(umsg));
    return 0;
  }
  }
  return 1;
}

uint8_t
u8x8_gpio_and_delay_mine(
u8x8_t *u8x8,
uint8_t msg,
uint8_t arg_int,
void *arg_ptr)
{

  switch(msg)
  {
    case U8X8_MSG_GPIO_AND_DELAY_INIT:
      /* only support for software I2C*/
      break;

    case U8X8_MSG_DELAY_NANO:
      /* not required for SW I2C */
      break;

    case U8X8_MSG_DELAY_10MICRO:
      /* not used at the moment */
      break;

    case U8X8_MSG_DELAY_100NANO:
      /* not used at the moment */
      break;

    case U8X8_MSG_DELAY_MILLI:
      //delay_micro_seconds(arg_int*1000UL);
      break;

    case U8X8_MSG_DELAY_I2C:
      /* arg_int is 1 or 4: 100KHz (5us) or 400KHz (1.25us) */
      //delay_micro_seconds(arg_int<=2?5:1);
      break;

    case U8X8_MSG_GPIO_I2C_CLOCK:
      break;

    case U8X8_MSG_GPIO_I2C_DATA:
      break;
/*
    case U8X8_MSG_GPIO_MENU_SELECT:
      u8x8_SetGPIOResult(u8x8, Chip_GPIO_GetPinState(LPC_GPIO, KEY_SELECT_PORT, KEY_SELECT_PIN));
      break;
    case U8X8_MSG_GPIO_MENU_NEXT:
      u8x8_SetGPIOResult(u8x8, Chip_GPIO_GetPinState(LPC_GPIO, KEY_NEXT_PORT, KEY_NEXT_PIN));
      break;
    case U8X8_MSG_GPIO_MENU_PREV:
      u8x8_SetGPIOResult(u8x8, Chip_GPIO_GetPinState(LPC_GPIO, KEY_PREV_PORT, KEY_PREV_PIN));
      break;

    case U8X8_MSG_GPIO_MENU_HOME:
      u8x8_SetGPIOResult(u8x8, Chip_GPIO_GetPinState(LPC_GPIO, KEY_HOME_PORT, KEY_HOME_PIN));
      break;
*/
    default:
      //u8x8_SetGPIOResult(u8x8, 1);
      break;
  }
  return 1;
}

uint8_t USER_SetFrequency(void){
	int timerCounter =__HAL_TIM_GET_COUNTER(&htim1);

	//use the encoder, to step through setting
	switch (timerCounter) {
		case 0:
			//set to 1kHz
			break;

		case 1:
			//set to 10kHz
			 break;


		case 3:
			//set to 100kHz
			break;

		default:
			break;
	}

	  return 1;
}

uint8_t USER_UpdatePWM(void){

/*
Set timer prescaller
Timer count frequency is set with
timer_tick_frequency = Timer_default_frequency / (prescaller_set + 1)

    timer_tick_frequency = 80000000 / (0 + 1) = 80000000
    //check the clock frequency

PWM_frequency = timer_tick_frequency / (TIM_Period + 1)
If you know your PWM frequency you want to have timer period set correct
TIM_Period = timer_tick_frequency / PWM_frequency - 1

	for 10Khz PWM_frequency, set Period to
	TIM_Period = 80000000 / 10000 - 1 = 7999
*/

	uint16_t iPSC =adcData[0];

	if (iSetFreq+2 < iPSC || iSetFreq-2 > iPSC) {
		int iRELOAD = 500;

		//pulse_length = ((TIM_Period + 1) * DutyCycle) / 100 - 1
		//where DutyCycle is in percent, between 0 and 100%
		int iCCR = ((iRELOAD + 1) * iDUTY) / 100 - 1;

		iSetFreq = iPSC;  // nicer to conver to actual

	  __HAL_TIM_SET_PRESCALER(&htim4, iPSC);
	  __HAL_TIM_SET_AUTORELOAD(&htim4, iRELOAD);
	  __HAL_TIM_SET_COMPARE(&htim4, TIM_CHANNEL_1, iCCR);
	}

	return 1;

}

void Refresh_OLED(u8g2_t *u8g2, IMGS img, char* data)
{
  u8g2_ClearBuffer(u8g2);
  uint32_t curtime = HAL_GetTick();

  	  u8g2_ClearBuffer(u8g2);

        u8g2_SetFont(u8g2, u8g2_font_ncenB14_tr);
        u8g2_DrawStr(u8g2, 0,14,"SIGNALv1!");

        u8g2_uint_t widthStr =0;


        u8g2_SetFontMode(u8g2,1);
        u8g2_SetDrawColor(u8g2,2);

        int tC =__HAL_TIM_GET_COUNTER(&htim1)>>2;

        	enum {BufSize=9};
		  char buf[BufSize];
		  snprintf (buf, BufSize, "%d",  tC );

		u8g2_SetFont(u8g2, u8g2_font_inb19_mf); //u8g2_font_fub20_tn);
		widthStr = u8g2_DrawStr(u8g2, 0,40, buf); //data
        u8g2_DrawBox(u8g2, 0,19,widthStr,25);



        u8g2_DrawBox(u8g2, 120,0,10,tC);
		 u8g2_SendBuffer(u8g2);

}

void OLD_Refresh_OLED(u8g2_t *u8g2, IMGS img, char* data)
{
  u8g2_ClearBuffer(u8g2);
  uint32_t curtime = HAL_GetTick();

  int pagenum = 0;
  u8g2_FirstPage(u8g2);
  do {
    pagenum++;

    //u8g2_SetContrast(u8g2, 255);
    switch (img) {
    case HI_WORLD:
      u8g2_SetFont(u8g2, u8g2_font_ncenB14_tr);
      u8g2_DrawStr(u8g2, 0,14,"SIGNALv1!");
      u8g2_DrawStr(u8g2, 0,32,"Init");
      break;

    case STOCK_U8G2:

      break;

    case TIMECODE:
        u8g2_SetFont(u8g2, u8g2_font_ncenB14_tr);
        u8g2_DrawStr(u8g2, 0,14,"SIGNALv1!");


		u8g2_SetFont(u8g2, u8g2_font_fub35_tn);
		u8g2_DrawStr(u8g2, 0,64, data);



      break;


    case LINES: {
      int steps = 16;
      int dx = 128/steps;
      int dy = 64/steps;
      int y = 0;
      for(int x=0; x<128; x+=dx) {
        u8g2_DrawLine(u8g2, x, 0, 127, y);
        u8g2_DrawLine(u8g2, 127-x, 63, 0, 63-y);
        y+=dy;
      }

      break;
    }

    default:
      break;

    }

    //NextPage calls SendBuffer
    //u8g2_SendBuffer(u8g2);

    // when drawing the time, lets just do the first two pages
    if (img==TIMECODE && pagenum>=2) {
      //return;
    }
  } while ( u8g2_NextPage(u8g2) );

}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void _Error_Handler(char * file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1) 
  {
  }
  /* USER CODE END Error_Handler_Debug */ 
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
